This docker image extends the gitlab/gitlab-runner:alpine image and prepends the normal `gitlab-runner` startup with a `gitlab-runner register` command. This can be useful for deploying a container image to a spot VM. Credit to https://hub.docker.com/r/sgillespie/gitlab-runner for giving me the idea. This approach has the advantage of no additional bash scripts or layers added, for a small and lightweight container.

You must specify environment variables to inform how the runner will register. Names of variables can be found by running `gitlab-runner register help`.

```bash
docker run --rm \
--name auto-registered-runner \
-e REGISTER_NON_INTERACTIVE=true \
-e CI_SERVER_URL=https://gitlab.com \
-e RUNNER_NAME=<RUNNER NAME> \
-e RUNNER_EXECUTOR=docker \
-e DOCKER_IMAGE=ruby \
-e REGISTRATION_TOKEN=<REGISTRATION TOKEN HERE> \
registry.gitlab.com/jrreid/gitlab-runner-autoregister:latest
```
