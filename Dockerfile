FROM gitlab/gitlab-runner:alpine
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/bin/bash", "-c", "gitlab-runner register && exec /entrypoint run --user=gitlab-runner --working-directory=/home/gitlab-runner"]
